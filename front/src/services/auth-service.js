import axios from 'axios';

const API_URL = 'http://209.97.147.143:8083/api/autenticacion/';

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'iniciar', {
        correo: user.correo,
        pass: user.pass
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }
  /*
  register(user) {
    return axios.post(API_URL + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
  */
}

export default new AuthService();
