import Vue from 'vue'
import Router from 'vue-router'
import Dogs from './views/Dogs.vue'
import login from './views/login.vue'
import search from './views/search.vue'
import createAd from './views/createAd.vue'
import ranking from './views/ranking.vue'
Vue.use(Router);
var router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Dogs
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/search/:type',
      component: search,
    },
    {
      path: '/create',
      component: createAd,
    },
    {
      path: '/ranking',
      component: ranking,
    }
  ]
})

export default router;
