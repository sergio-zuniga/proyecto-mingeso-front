import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store/index.js'
import axios from 'axios';
import 'bootstrap';
import router from './router';
import 'vuetify/dist/vuetify.min.css'; // Ensure you are using css-loader
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import VeeValidate from 'vee-validate';


require("./assets/style/style.scss");

const axiosInstance = axios.create({ //configurar para backend
})

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
window.$ = window.jQuery = require("jquery")
Vue.prototype.$http = axiosInstance;

Vue.config.productionTip = false;
//Paginación
import Pagination from 'vue-pagination-2';
Vue.component('pagination', Pagination);
Vue.use(VeeValidate);
new Vue({
  router,
  store,
  axios,
  vuetify,
  render: h => h(App)
}).$mount('#app')

